const express = require('express');
const cors = require('cors');

const { dbConection } = require('../database/config-db');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.routePaths = {
      auth: '/auth',
      adn: '/api/adn',
    };

    this.conectDB();

    this.middlewares();

    this.routes();
  }

  async conectDB() {
    await dbConection();
  }

  middlewares() {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.routePaths.auth, require('../routes/auth.routes'));
    this.app.use(this.routePaths.adn, require('../routes/adn.routes'));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`Servidor corriendo en http://localhost:${this.port}`);
      this.app.emit('ready');
    });
  }

}

module.exports = Server;
