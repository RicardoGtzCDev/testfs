const { response, request } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user.model');
const { getJWT } = require('../helpers/jwt-manager');

const login = async (req = request, res = response) => {
  const { nombre, password } = req.body;
  try {
    const usuario = await User.findOne({ nombre });
    if (!usuario) {
      return res.status(400).json({
        message: `Usuario ( ${nombre} ) no registrado.`,
      });
    }
    if (!bcrypt.compareSync(password, usuario.password)) {
      return res.status(400).json({
        message: 'Contraseña incorrecta.',
      });
    }

    const jwt = await getJWT(usuario.id);

    res.json({
      message: 'Login OK',
      payload: {
        usuario,
        jwt,
      },
    });
  } catch (error) {
    res.status(500).json({
      message: `Ha ocurrido un error '${error}'. Contacte al administrador.`,
    });
  }
};


module.exports = { login };
