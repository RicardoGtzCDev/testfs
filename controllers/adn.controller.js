const { response, request } = require('express');

const Adn = require('../models/adn.model');

const { existeAdn } = require('../helpers/adn-db');
const { hasMutation } = require('../helpers/mutations');

const checkMutation = async (req = request, res = response) => {
  const { dna } = req.body;
  let muta;
  try {
    const adnId = dna.join('');
    const registro = await existeAdn(adnId);
    if (registro.existe) {
      muta = registro.modeloAdn.hasMutation;
    } else {
      muta = await hasMutation(dna);
      const data = {
        dna: adnId,
        hasMutation: muta,
      };
      const adn = new Adn(data);
      await adn.save();
    }
    if (muta) {
      res.status(200).end();
    } else {
      res.status(403).end();
    }
  } catch (error) {
    res.status(500).json({
      error: `Algo ha salido mal (${error}). Contacta al administrador del sistema.`,
    });
  }
};

const getStats = async (req = request, res = response) => {
  try {
    const [con, sin] = await Promise.all([
      Adn.countDocuments({ hasMutation: true }),
      Adn.countDocuments({ hasMutation: false }),
    ]);

    res.status(200).json({
      count_mutations: con,
      count_no_mutation: sin,
      ratio: con / sin,
    });
  } catch (error) {
    res.status(500).json({
      error: `Algo ha salido mal (${error}). Contacta al administrador del sistema.`,
    });
  }
};

module.exports = { checkMutation, getStats };
