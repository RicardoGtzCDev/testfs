const { Router } = require('express');
const { body } = require('express-validator');

const { validateFields } = require('../middlewares/validate_fields');
const { login } = require('../controllers/auth.controller');

const router = Router();

router.post(
  '/login',
  [
    body('nombre', 'El nombre es obligatorio').notEmpty(),
    body('password', 'La constraseña es obligatoria').notEmpty(),
    validateFields,
  ],
  login
);

module.exports = router;
