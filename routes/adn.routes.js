const { Router } = require('express');
const { body, header } = require('express-validator');

const { checkMutation, getStats } = require('../controllers/adn.controller');
const { validateJWT } = require('../middlewares/validate-jwts');

const { validateFields } = require('../middlewares/validate_fields');

const router = Router();

router.get('/stats', [validateJWT, validateFields], getStats);

router.post(
  '/mutation',
  [
    validateJWT,
    body('dna')
      .exists()
      .bail()
      .withMessage('No se ha enviado la secuencia de ADN')
      .isArray()
      .withMessage(`La secuencia de ADN no se envió en el formato correcto.`),
    validateFields,
  ],
  checkMutation
);

module.exports = router;
