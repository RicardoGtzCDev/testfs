const mongoose = require('mongoose');

const credenciales = process.env.MONGODB;

const dbConection = async () => {
  try {
    await mongoose.connect(credenciales, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log('Base de datos ONLINE');
  } catch (error) {
    console.log('Base de datos OFFLINE');
    throw new Error(`Error en base de datos: ${error}`);
  }
};

module.exports = { dbConection };
