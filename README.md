# Prueba FullStack Dev. Guros
***
## Índice
1. Descripción
2. Instalación
3. Uso
4. Sandbox
5. Tests
6. Autor
7. Licencia
***

## 1) Descripción

Prueba Guros Full Stack.

El proyecto se centra en la resolución de los problemas que podemos encontrar en ```./assets/PFSG.pdf```.


## 2) Instalación

```bash
npm install -d
```
* ### Dependencias
    Las siguientes librerias de npm son requeridas para el funcionamiento, depuración y testeo de la aplicación.

    bcryptjs, chai, chai-http, cors, dotenv, express, express-validator, jsonwebtoken, mocha, mongoose, nyc

* ### Configuración de variables de entorno

  * Crear un archivo ```.env```. en la raiz del proyecto.
  * Dentro del archivo ```.env``` copiar el contenido del archivo ```.envexample```.
  * Asignar los valores correspondientes (valores adjuntos en el coreo) a cada variable de entorno.
  
> **_NOTA:_**  En el caso de la variable de entorno ```TestJWT```, esta es generada por el endpoint ```auth/login```, dicho jwt tiene un tiemo de expiración de 3h.


## 3) Uso

La aplicación cuenta con 3 servicios, 1 ```(auth/login)``` de autenticacion  y 2 ```(api/adn)``` que resuelven los problemas planteados en la documentación adjunta de la sección descripción.

> Para iniciar la aplicación:
    ```
    npm start || node app || nodemon
    ```

* ### Auth
  * #### login 

    Nos proporciona un token de autenticación ```( JWT )``` de un usuario a traves de su ```username``` y su ```password```.

    * *Tipo:* POST
    * *Ruta:* url/auth/login
    * *Parámetros*
      * *(body):* 
        ```json
        {
            "nombre":"string",
            "password":"string"
        }
        ```
    * *Respuesta:* 
        ```json
        {
            "message": "string",
            "payload": {
                "usuario": {
                    "_id": "string",
                    "nombre": "string"
                },
                "jwt": "string"
        }
        ```

* ### Adn
  * #### mutation 

    Valida si la cadena dada de ADN tiene mas de 1 mutación.

    * *Tipo:* POST
    * *Ruta:* url/api/adn/mutation
    * *Parámetros* 
      * *(body)*: 
        ```json
        {
            "dna": [
                "string",
                ...,
                "string"
            ]
        }
        ```
      * *(headers):* 
        ```
        key = _tkn
        value = ***jwt obtenido de auth/login***
        ```
    * *Respuesta:* 
        ```
        status 200 en caso que la cadena de ADN SI tenga mutaciones.
        status 403 en caso que la cadena de ADN NO tenga mutaciones.
        ```
  * #### stats 

    Nos proporciona 3 indicadores con la comparativa de los ADN´s validados.

    * *Tipo:* GET
    * *Ruta:* url/api/adn/STATS
    * *Parámetros:* 
        * (headers):* 
        ```
        key = _tkn
        value = ***jwt obtenido de auth/login***
        ```
    * *Respuesta:* 
        ```json
        {
            "count_mutations": number,
            "count_no_mutation": number,
            "ratio": number
        }
        ```
## 4) Sandbox

### Postman
    url: https://documenter.getpostman.com/view/15553038/TzRX8k55

### Heroku
    url: https://testfs-guro.herokuapp.com/

## 5) Tests

```st
npm test
```
## 6) Autor
* *Nombre:* testfs
* *Autor:* Ricardo Gutiérrez Cañas
* Creado:* 16/05/2021
* Versión:* 1.0.0


## 7) Licencia

    ISC


