const { validationResult } = require('express-validator');

const validateFields = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let setObj = new Set();
    let errores = errors.errors.reduce((acc, item) => {
      if (!setObj.has(item.id)) {
        setObj.add(item.id, item);
        acc.push(item);
      }
      return acc;
    }, []);
    return res.status(400).json({
      message: `Han ocurrido uno o mas errores, consulte 'errors' para mas detalle.`,
      errors: errores,
    });
  }
  next();

};

module.exports = { validateFields };