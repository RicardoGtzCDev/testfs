const { request, response } = require('express');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

const validateJWT = async (req = request, res = response, next) => {
  const token = req.header('_tkn');
  try {
    if (!token) {
      return res.status(401).json({
        message: 'No hay token en la petición.',
      });
    }
    const { uid } = jwt.verify(token, process.env.SECRETJWT);
    const usuario = await User.findById(uid);
    if (!usuario) {
      return res.status(401).json({
        message: 'El usuario autenticado no existe.',
      });
    }
    req.usuarioAuth = usuario;
    next();
  } catch (error) {
    return res.status(401).json({
      message: `Algo ha salido mal. JWT no válido ( ${error} ).`,
    });
  }
};

module.exports = { validateJWT };
