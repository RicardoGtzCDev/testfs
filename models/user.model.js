const { Schema, model } = require('mongoose');

const UsuarioSchema = Schema({
  nombre: {
    type: String,
    required: [true, 'El nombre es obligatorio'],
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio'],
  },
});

UsuarioSchema.methods.toJSON = function () {
  const { password, ...resto } = this.toObject();
  return resto;
};

module.exports = model('User', UsuarioSchema);
