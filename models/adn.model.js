const { Schema, model } = require('mongoose');

const AdnSchema = Schema({
  dna: {
    type: String,
    unique: true,
    required: true,
    default: [],
  },
  hasMutation: {
    type: Boolean,
    required: true,
    default: false,
  },
});

module.exports = model('Adn', AdnSchema);
