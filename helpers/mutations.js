const Adn = require('../models/adn.model');

const hasMutation = async (dna = []) => {
  try {
    const cadenas = await chainsBuilder(dna);
    const mutaciones = await chainsFilter(cadenas);
    return mutaciones > 1 ? true : false;
  } catch (error) {
    throw new Error(
      `Error al comprobar mutaciones de la cadena de ADN, '${error}'`
    );
  }
};

const chainsBuilder = async (dna) => {
  const numRow = dna.length;
  const numCol = dna[0].length;
  const numDI = -numRow + 1;
  let rows = new Array(numRow);
  let cols = new Array(numCol);
  let diagonalD = new Array(numCol + numRow - 1);
  let diagonalI = new Array(diagonalD.length);
  // el primer elemento siempre será undefined
  // retorna NAN en caso de que falten chars en alguna de las cadenasa de texto
  for (let x = 0; x < numCol; x++) {
    for (let y = 0; y < numRow; y++) {
      const caracter = [...dna[y]][x];
      rows[y] += caracter;
      cols[x] += caracter;
      diagonalD[x + y] += caracter;
      diagonalI[x - y - numDI] += caracter;
    }
  }
  return [rows, cols, diagonalD, diagonalI];
};

const chainsFilter = async (chains = []) => {
  const regE = new RegExp(/undefined/g);
  const pattern = ['AAAA', 'TTTT', 'CCCC', 'GGGG'];
  let conteo = 0;
  chains.forEach((array) => {
    array.forEach((element) => {
      element = element.replace(regE, '');
      if (pattern.some((patron) => element.includes(patron))) {
        conteo++;
      }
    });
  });
  return conteo;
};

module.exports = { hasMutation };
