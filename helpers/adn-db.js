const Adn = require('../models/adn.model');

const existeAdn = async (adn) => {
  const query = { dna: adn };
  const existe = await Adn.exists(query);
  if (existe) {
    const modeloAdn = await Adn.findOne(query);
    return { existe, modeloAdn };
  }
  return { existe: false };
};

module.exports = { existeAdn };
