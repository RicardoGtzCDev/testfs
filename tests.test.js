let chai = require('chai');
const assert = require('chai').assert;
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

require('dotenv').config();
const Server = require('./Server/server');
const server = new Server();
server.listen();
const envi = 'http://localhost:' + process.env.PORT;
const urlAdn = envi + '/api/adn';
const urlAuth = envi + '/auth';

const { hasMutation } = require('./helpers/mutations');

describe('Test de FSGuros', function () {
  describe('Funcion hasMutations. Determina si el ADN tien 2 o mas mutaciones', function () {
    it('hasMutation. True or False', async function () {
      // ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG'] => true
      // ['ATGCGC', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'] => false
      const result = await hasMutation([
        'ATGCGA',
        'CAGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCTA',
        'TCACTG',
      ]);
      assert.typeOf(result, 'boolean');
    });
  });
  describe('Controlador de los endpoints de adn', function () {
    it('CheckMutation. True. De ser necesario graba en BD', function (done) {
      chai
        .request(urlAdn)
        .post('/mutation')
        .set('_tkn', process.env.TestJWT)
        .send({
          dna: ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG'],
        })
        .end(function (err, res) {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('CheckMutation. False. De ser necesario graba en BD', function (done) {
      chai
        .request(urlAdn)
        .post('/mutation')
        .set('_tkn', process.env.TestJWT)
        .send({
          dna: ['ATGCGC', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
        })
        .end(function (err, res) {
          expect(res).to.have.status(403);
          done();
        });
    });

    it('JSON. Consulta estadisticas de BD', function (done) {
      chai
        .request(urlAdn)
        .get('/stats')
        .set('_tkn', process.env.TestJWT)
        .end(function (err, res) {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
  describe('Controlador de los endpoints de auth', function () {
    it('Validación de JWT del usuario', function (done) {
      chai
        .request(urlAuth)
        .post('/login')
        .send({
          nombre: process.env.TestName,
          password: process.env.TestPass,
        })
        .end(function (err, res) {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
